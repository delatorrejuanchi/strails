module Strails
  class OptionType < ApplicationRecord
    has_many :option_values, dependent: :destroy

    validates_presence_of :name, :presentation
  end
end
