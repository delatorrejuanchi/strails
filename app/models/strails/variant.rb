module Strails
  class Variant < ApplicationRecord
    attr_accessor :destroyed_by_product

    belongs_to :product
    has_many :option_value_variants, dependent: :destroy
    has_many :option_values, through: :option_value_variants

    validates_presence_of :product
    validates_presence_of :option_values, unless: :is_master
    validates_inclusion_of :is_master, in: [true, false]

    before_save :set_default_sku, unless: :sku
    before_save :set_default_price, unless: :price
    before_destroy :dont_unless_destroyed_by_product, if: :is_master, prepend: true

    delegate :name, :description, to: :product

    private

    def set_default_sku
      return unless sku.nil?
      self.sku = ([name] + option_values.map(&:name)).join(' ').parameterize
    end

    def set_default_price
      return unless price.nil?
      self.price = is_master ? 0 : product.price
    end

    def dont_unless_destroyed_by_product
      unless destroyed_by_product
        errors.add(:base, :cant_be_destroyed, message: "can't be destroyed if it is the master variant of a product")
        throw(:abort)
      end
    end
  end
end
