require_dependency "strails/frontend_controller"

module Strails
  class Frontend::HomeController < FrontendController
    def index
    end
  end
end
