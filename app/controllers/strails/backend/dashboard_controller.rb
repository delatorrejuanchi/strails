require_dependency "strails/backend_controller"

module Strails
  class Backend::DashboardController < BackendController
    def index
    end
  end
end
