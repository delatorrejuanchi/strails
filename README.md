# Strails (Work in Progress)
Strails is a full-stack e-commerce framework for developing online stores using Ruby on Rails.

<!-- ## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'strails'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install strails
```

## Contributing
Contribution directions go here. -->

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
