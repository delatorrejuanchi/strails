require 'test_helper'

module Strails
  class Frontend::HomeControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    test "should get index" do
      get root_url
      assert_response :success
    end

  end
end
