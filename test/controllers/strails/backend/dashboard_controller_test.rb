require 'test_helper'

module Strails
  class Backend::DashboardControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    test "should get index" do
      get backend_root_url
      assert_response :success
    end

  end
end
